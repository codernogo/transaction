package com.transaction.transaction.model.dao;

import com.transaction.transaction.model.Type;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class TransactionInput {

    private String id;

    private String date;

    @Enumerated(EnumType.STRING)
    private Type type;

    private String description;

    private double debit;

    private double credit;

    private String balance;

}
